import { takeEvery, put, call } from "redux-saga/effects";
import {
  GET_USER_INFO_REQUESTED,
  GET_USER_INFO_SUCCESS,
  GET_USER_INFO_FAILURE
} from "../constants/constants";
import getUserInfoAPI from "../api/api_fetch_user";

function* getUserInfoSaga() {
  try {
    const result = yield call(getUserInfoAPI);

    console.log(result); //response.data
    //I could separate data here but going to push all response

    yield put({ type: GET_USER_INFO_SUCCESS, payload: result });
  } catch (err) {
    yield put({ type: GET_USER_INFO_FAILURE });
  }
}

export default function* rootSaga() {
  yield takeEvery(GET_USER_INFO_REQUESTED, getUserInfoSaga);
}
