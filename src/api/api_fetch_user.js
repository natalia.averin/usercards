import axios from "axios";
const API_URL = `https://uinames.com/api/?ext&amount=15&region=germany`;

const getUserInfoAPI = () =>
  axios
    .get(API_URL)
    .then(response => response.data)
    .catch(err => {
      throw err;
    });

export default getUserInfoAPI;
