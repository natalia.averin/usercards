import React, { Component } from "react";

//MaterliaUI
import { withStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import MenuItem from "@material-ui/core/MenuItem";

//FA icons
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMinus } from "@fortawesome/free-solid-svg-icons";
import { faPlus } from "@fortawesome/free-solid-svg-icons";

const styles = theme => ({
  header: {
    minHeight: 70
  },
  button: {},
  buttonSubmit: {},
  container: {
 
  },
  textField: {
    width: 200
  },
  menu: {
    width: 200
  }
});


class AddUserForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      formIsVisible: false,
      formUserData: {
        name: "",
        age: "",
        description: "",
        gender: "none",
        photo: "https://uinames.com/api/photos/female/8.jpg"
      }
    };
  }

  changeFormVisibility = () => {
    this.setState({
      formIsVisible: !this.state.formIsVisible
    });
  };

  onSelectChange = e => {
    const gender = e.target.value;
    this.setState({
      formUserData: {
        gender
      }
    });
  };

  onChangeForm = label => event => {
    const newState = {};
    newState[label] = event.target.value;
    this.setState({
      formUserData: { ...newState }
    });
  };

  resetFields = () => {
    const emptyUserForm = {
      name: "",
      age: "",
      description: "",
      gender: "",
      photo: "https://source.unsplash.com/random"
    };

    this.setState({
      formUserData: emptyUserForm
    });
  };

  onSubmitForm = event => {
    event.preventDefault();
    this.props.addUser(this.state.formUserData);
    this.resetFields();
  };

  render() {
    const { classes } = this.props;
    const formIsVisible = this.state.formIsVisible;
    const formStyle = {
      display: formIsVisible ? "block" : "none"
    };
    const plusOrMinusSign = formIsVisible ? (
      <FontAwesomeIcon icon={faMinus} color="pink" />
    ) : (
      <FontAwesomeIcon icon={faPlus} color="pink" />
    );

    const hideOrAddText = formIsVisible ? "Nah, later" : "I want to go";

    const selectValues = [
      {
        value: "female",
        label: "Female"
      },
      {
        value: "male",
        label: "Male"
      },
      {
        value: "idk",
        label: "Have no idea"
      },
      {
        value: "alient",
        label: "I am not from Earth"
      }
    ];

    return (
      <div>
        <Grid
          container
          spacing={8}
          alignItems="center"
          className={classes.header}
        >
          <Grid item>
            <Button
              variant="contained"
              color="secondary"
              size="large"
              className={classes.button}
              onClick={this.changeFormVisibility}
            >
              {hideOrAddText} {plusOrMinusSign}
            </Button>
          </Grid>
          <Grid item>
            <form
              style={formStyle}
              className={classes.container}
              autoComplete="off"
              onSubmit={this.onSubmitForm}
            >
              <Grid container spacing={8} alignItems="center">
                <Grid item>
                  <TextField
                    item
                    id="name"
                    label="Name"
                    value={this.state.formUserData.name}
                    onChange={this.onChangeForm("name")}
                    className={classes.textField}
                    variant="outlined"
                  />
                </Grid>
                <Grid item>
                  <TextField
                    id="age"
                    label="Age"
                    value={this.state.formUserData.age}
                    onChange={this.onChangeForm("age")}
                    className={classes.textField}
                    variant="outlined"
                  />
                </Grid>

                <Grid item>
                  <TextField
                    id="outlined-select-options"
                    select
                    label="Gender"
                    value={this.state.formUserData.gender}
                    onChange={this.onSelectChange}
                    className={classes.textField}
                    SelectProps={{
                      MenuProps: {
                        className: classes.menu
                      }
                    }}
                    variant="outlined"
                  >
                    {selectValues.map(option => (
                      <MenuItem key={option.value} value={option.value}>
                        {option.label}
                      </MenuItem>
                    ))}
                  </TextField>
                </Grid>

                <Grid item>
                  <TextField
                    id="description"
                    label="Description"
                    multiline
                    rowsMax="4"
                    value={this.state.formUserData.description}
                    onChange={this.onChangeForm("description")}
                    className={classes.textField}
                    variant="outlined"
                  />
                </Grid>

                <Grid item>
                  <Button
                    variant="contained"
                    color="secondary"
                    size="large"
                    type="submit"
                    className={classes.buttonSubmit}
                  >
                    Submit
                  </Button>
                </Grid>

                <Grid item>
                  <Button
                    size="medium"
                    color="primary"
                    className={classes.buttonSubmit}
                    onClick={this.resetFields}
                  >
                    Clear
                  </Button>
                </Grid>
              </Grid>
            </form>
          </Grid>
        </Grid>
      </div>
    );
  }
}

// AddUserForm.propTypes = {
//   name: PropTypes.string,
//   age: PropTypes.number,
//   description: PropTypes.string
// };

export default withStyles(styles)(AddUserForm);
