import React, { Component } from "react";
//import ISO from "../api/ISO";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faVenus } from "@fortawesome/free-solid-svg-icons";
import { faMars } from "@fortawesome/free-solid-svg-icons";
import { faHeart } from "@fortawesome/free-solid-svg-icons";
import { withStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
//import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";

const styles = {
  card: {
    maxWidth: 345,
    margin: 20
  },
  media: {
    height: 300,
    objectFit: "cover"
  }
};

class UserCard extends Component {
  onRemoveUser() {
    this.props.removeUser(this.props.currentUser);
    console.log("OnRemoveUser");
  }

  render() {
    const { photo, email, name, gender, region, age } = this.props.user;
    //const flagClassName = `flag-icon flag-icon-${ISO(region)}`;
    const genderSign =
      gender === "female" ? (
        <FontAwesomeIcon icon={faVenus} color="pink" />
      ) : (
        <FontAwesomeIcon icon={faMars} color="#33ccff" />
      );
    const { classes } = this.props;
    const cardo = (
      <div>
        <Card className={classes.card}>
          <CardMedia
            component="img"
            alt="Contemplative Reptile"
            className={classes.media}
            height="140"
            image={photo}
            title="User Photo"
          />
          <CardContent>
            <Typography gutterBottom variant="h5" component="h2">
              {name} {age} {genderSign}
            </Typography>
            <Typography component="p">
              Lizards are a widespread group of squamate reptiles, with over
              6,000 species, ranging across all continents except Antarctica
            </Typography>
            <br />
            <Typography component="p">
              {" "}
              {region} a widespread group of squamate reptiles, with over 6,000
              species, ran
            </Typography>
          </CardContent>
          <p className="card-text">
            <small className="text-muted"> </small>{" "}
          </p>

          <CardActions>
            <Button size="small" color="primary">
              <FontAwesomeIcon icon={faHeart} color="grey" />
            </Button>
            <Button
              size="small"
              color="primary"
              onClick={this.onRemoveUser.bind(this)}
            >
              Remove
            </Button>
          </CardActions>
        </Card>
      </div>
    );

    return cardo;
  }
}

export default withStyles(styles)(UserCard);
