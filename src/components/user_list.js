import React, { Component } from "react";
import UserCard from "./user_card";
import Grid from "@material-ui/core/Grid";

export default class UserList extends Component {
  render() {
    const cards = this.props.users.userData.map((user, index) => (
      <UserCard
        key={index}
        user={user}
        removeUser={this.props.removeUser}
        currentUser={user}
      />
    ));
    return (
      <Grid container direction="row" justify="center" alignItems="center">
        {cards}
      </Grid>
    );
  }
}
