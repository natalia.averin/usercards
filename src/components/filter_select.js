import React from "react";
import PropTypes from "prop-types";

import { withStyles } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";

const styles = theme => ({
  header: {},
  formControl: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 100
  },
  selectEmpty: {
    // marginTop: theme.spacing.unit * 2
  },
  root: {
    display: "flex",
    flexWrap: "wrap"
  }
});

class FilterSelect extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      filterBy: "all"
    };
  }

  onSelectChange = e => {
    const filterBy = e.target.value;

    this.setState({
      filterBy
    });
    this.props.filterCards(filterBy);
  };

  render() {
    const { classes } = this.props;

    return (
      <form itemclassName={classes.root} autoComplete="off">
        <FormControl className={classes.formControl}>
          <InputLabel shrink htmlFor="age-label-placeholder">
            Filter
          </InputLabel>
          <Select
            value={this.state.filterBy}
            onChange={this.onSelectChange}
            inputProps={{
              name: "gender",
              id: "gender"
            }}
            className={classes.selectEmpty}
          >
            <MenuItem value="all">
              <em>All</em>
            </MenuItem>
            <MenuItem value="female">Female</MenuItem>
            <MenuItem value="male">Male</MenuItem>
          </Select>
        </FormControl>
      </form>
    );
  }
}

FilterSelect.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(FilterSelect);
