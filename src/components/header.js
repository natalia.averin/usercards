import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";

//MaterliaUI
import Grid from "@material-ui/core/Grid";

const styles = {
  header: {}
};

class Header extends Component {
  render() {
    return (
      <div>
        <Grid className={this.props.classes.header}>
          <h2>
            {this.props.amount} people attend to {this.props.eventName}
          </h2>
        </Grid>
      </div>
    );
  }
}

export default withStyles(styles)(Header);
