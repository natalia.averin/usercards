import { ON_SUBMIT } from "../constants/constants";

export function onSubmit(data) {
  return {
    type: ON_SUBMIT,
    payload: data
  };
}
