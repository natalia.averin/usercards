import { FILTER_CARDS } from "../constants/constants";

export function filterCards(filterBy) {
  return {
    type: FILTER_CARDS,
    payload: filterBy
  };
}
