import { GET_USER_INFO_REQUESTED } from "../constants/constants";

export function getUserInfo(data) {
  return {
    type: GET_USER_INFO_REQUESTED,
    payload: data
  };
}
