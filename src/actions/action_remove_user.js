import { REMOVE_USER_FROM_LIST } from "../constants/constants";

export function removeUser(currentUser) {
  return {
    type: REMOVE_USER_FROM_LIST,
    payload: currentUser
  };
}
