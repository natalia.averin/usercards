import { ADD_USER } from "../constants/constants";

export function addUser(newUser) {
  return {
    type: ADD_USER,
    payload: newUser
  };
}
