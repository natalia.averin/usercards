import { SEARCH_CARDS } from "../constants/constants";

export function searchCards(searchBy) {
  return {
    type: SEARCH_CARDS,
    payload: searchBy
  };
}
