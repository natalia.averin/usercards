import {
  GET_USER_INFO_REQUESTED,
  GET_USER_INFO_SUCCESS,
  GET_USER_INFO_FAILURE,
  REMOVE_USER_FROM_LIST,
  ADD_USER,
  FILTER_CARDS,
  SEARCH_CARDS
} from "../constants/constants";

import { without, filter } from "lodash";

export default function(state = { loading: true }, action) {
  switch (action.type) {
    case GET_USER_INFO_REQUESTED:
      return { loading: true, ...state };
    case GET_USER_INFO_SUCCESS:
      return {
        loading: false,
        userData: action.payload,
        unfilteredUserData: action.payload
      };
    case GET_USER_INFO_FAILURE:
      return { loading: false, ...state };
    case REMOVE_USER_FROM_LIST:
      const userDataWithoutCurrentUser = without(
        state.userData,
        action.payload
      );
      return { ...state, userData: userDataWithoutCurrentUser };
    case ADD_USER:
      const newUserData = state.userData;
      newUserData.unshift(action.payload);

      return { ...state, userData: newUserData };

    case FILTER_CARDS:
      const filterBy = action.payload; //"male"

      if (filterBy !== "all") {
        const filteredUsers = filter(
          state.unfilteredUserData,
          user => user.gender === filterBy
        );
        return { ...state, userData: filteredUsers };
      }

      return { ...state, userData: state.unfilteredUserData };
    case SEARCH_CARDS:
      const searchBy = action.payload; //"Vasja"

      const newUsers = [];
      state.unfilteredUserData.forEach(user => {
        if (user.name.toLowerCase().includes(searchBy)) {
          newUsers.push(user);
        }
      });
      return { ...state, userData: newUsers };

    default:
      return state;
  }
}
