import { combineReducers } from "redux";
import UserReducer from "./reducer_users";
import EventReducer from "./reducer_event";

const rootReducer = combineReducers({
  users: UserReducer,
  event: EventReducer
});

export default rootReducer;
