import { CHANGE_FORM, RESET_FORM } from "../constants/constants";

const photoURL = "https://source.unsplash.com/random";

const INITIAL_STATE = {
  name: "",
  age: "",
  description: "",
  selectedGender: "Female"
};

export default function(state = INITIAL_STATE, action) {
  switch (action.type) {
    case CHANGE_FORM:
      const { label, value } = action.payload;
      //  label = "name, age, gender"
      //  value = "Vasja, 24, male"
      const newState = {};
      newState[label] = value;

      return {
        ...state,
        ...newState,
        // [label]: value,
        photo: photoURL
      };
    case RESET_FORM:
      //const {name, age, description, gender} = action.payload;

      console.log("Bazinga1");
      console.log(action.payload);
      return { ...state, ...action.payload };
    default:
      return state;
  }
}
