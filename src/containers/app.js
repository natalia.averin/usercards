import React, { Component } from "react";
import { connect } from "react-redux";

//Actions
import { getUserInfo } from "../actions/action_users";
import { removeUser } from "../actions/action_remove_user";
import { addUser } from "../actions/action_add_user";
import { filterCards } from "../actions/action_filter_cards";
import { searchCards } from "../actions/action_search_cards";

//MaterliaUI
import { withStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import LinearProgress from "@material-ui/core/LinearProgress";

//myComponents:
import UserList from "../components/user_list";
import Header from "../components/header";
import AddUserForm from "../components/adduser_form";
import FilterSelect from "../components/filter_select";
import SearchInput from "../components/search_input";

//font-awesome library
import { library } from "@fortawesome/fontawesome-svg-core";
import { fab, faThemeco } from "@fortawesome/free-brands-svg-icons";
import { faCheckSquare, faCoffee } from "@fortawesome/free-solid-svg-icons";
library.add(fab, faCheckSquare, faCoffee);

const styles = theme => ({
  myCont: {
    marginTop: theme.spacing.unit * 3,
    paddingLeft: theme.spacing.unit * 5,
    paddingRight: theme.spacing.unit * 5
  }
});

class App extends Component {
  componentDidMount() {
    this.props.getUserInfo();
  }

  render() {
    if (this.props.users.loading === true) {
      return (
        <div>
          <LinearProgress /> loading...
        </div>
      );
    }

    return (
      <div>
        <Grid
          container
          direction="row"
          alignItems="center"
          className={this.props.classes.myCont}
        >
          <Grid item lg={6} sm={6}>
            <Header
              amount={this.props.users.userData.length}
              eventName={this.props.event.eventName}
            />
          </Grid>
          <Grid item lg={6} sm={6}>
            <Grid container justify="flex-end" alignItems="center" spacing={8}>
              <FilterSelect item filterCards={this.props.filterCards} />
              <SearchInput item searchCards={this.props.searchCards} />
            </Grid>
          </Grid>
          <AddUserForm addUser={this.props.addUser} />
          <UserList
            key="unique"
            users={this.props.users}
            removeUser={this.props.removeUser}
          />{" "}
        </Grid>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return { users: state.users, event: state.event }; // {weather} === { weather : weather} ES6 way
}

export default connect(
  mapStateToProps,
  {
    getUserInfo,
    removeUser,
    addUser,
    filterCards,
    searchCards
  }
)(withStyles(styles)(App));
